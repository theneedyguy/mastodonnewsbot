FROM python:3.7-alpine3.8

RUN apk add build-base bash

WORKDIR /app
 
COPY app.py /app
COPY requirements.txt /app
RUN pip install -r requirements.txt

ENTRYPOINT ["bash","-c", "while true; do './app.py'; sleep 600; done"]

