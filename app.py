#!/usr/bin/env python3
import requests
import configparser
import hashlib
import syslog
import logging
import os.path
import os
import time
import feedparser

logging.basicConfig(format='[%(asctime)s]-%(levelname)s -- %(message)s', level=logging.DEBUG)

def log(message):
    logging.info(message)


def tootit(title, descr, link, news_id):
    host_instance = 'https://newsbots.eu'
    token = os.getenv("TOKEN")

    headers = {}
    headers['Authorization'] = 'Bearer ' + token
    headers['Idempotency-Key'] = news_id

    data = {}
    data['status'] =  f"**{title}**\n\n{descr}\n{link}\n\n#news #bot" 
    data['visibility'] = 'public'

    response = requests.post(
        url=host_instance + '/api/v1/statuses', data=data, headers=headers)

    if response.status_code == 200:
        print("Posted")
        return True
    else:
        print("Not posted")
        return False


def checkNews(url):
    # Logging.
    log("Checking for new news")
    # Load cache
    cachefile = os.path.expanduser(".news_cache")
    cache = configparser.ConfigParser()
    cache.read(cachefile)
    # Fetch the feed.
    feed = feedparser.parse(url)

    # Ensure there's a section in the cache.
    try:
        cache.add_section("entries")
    except:
        pass
    
    news = []

    for item in feed['items']:

        title = item['title']
        identifier = item['id']
        print(identifier)
        key = hashlib.md5(identifier.encode("ascii")).hexdigest()
        link = item['links'][0]['href']
        descr = item["description"]
        
        try:
            cache.get("entries", key)
        except:
            log("Posting news '%s'..." % title)
            try:
                cache.set("entries", key, "1")
                tootit(title, descr, link, identifier)
                with open(cachefile, 'w+') as outputfile:
                    cache.write(outputfile)
                    outputfile.close()
                    news.append(title)
            except Exception as e:
                log("Error: '%s'" % str(e))
                log("Unable to post '%s'" % link)

checkNews(os.getenv("FEED"))
log("Waiting 10m for next check")

